# alignment

RAFT alignment module

## Workflows

# manifest_to_alns

Given a manifest, generate alignments using user-provided parameters.

Nested workflow design:
```
manifest_to_alns
  \
 raw_fqs_to_alns
   \
  procd_fqs_to_alns
    \
   alns_to_procd_alns
```

Require inputs:
| Parameter | Allowed values | Explanation |
| ----- | ----- | ----- |
| MANIFEST  | RAFT manifest | Provides sample-level information for processing |
| params.alignment\$manifest_to_alns\$fq_trim_tool | `fastp`, `trim_galore`, `trimmomatic` | FASTQ trimming tool |
| params.alignment\$manifest_to_alns\$fq_trim_tool_parameters | key:value hash (see documentation) | FASTQ trimming tool parameters |
| params.alignment\$manifest_to_alns\$aln_tool | `star`, `bwa`, `bbmap` | Alignment tool |
| params.alignment\$manifest_to_alns\$aln_tool_parameters | key:value hash (see documentation) | Alignment tool parameters |
| params.alignment\$manifest_to_alns\$aln_ref | A reference FASTA | Reference FASTA for alignment | 
| params.alignment\$manifest_to_alns\$gtf | A GTF | GTF used for informing alignments |
| params.alignment\$manifest_to_alns\$alt_ref | A reference FASTA | Alternate reference FASTA for alignment (e.g. transcriptomic ) |

Emitted channels:
| Channel Name | Explanation | 
| ----- | ----- |
| fqs | Raw FASTQS |
| procd_fqs | Process FASTQS |
| alns | Alignents |
| alt_alns | Alternate Alignments (e.g. --transcriptomeBAM from `STAR`) |
| junctions | Junctions, if generated |

# raw_fqs_to_alns

Given raw FASTQs, generate alignments using user-provided parameters.

Nested workflow design:
```
raw_fqs_to_alns
  \
 procd_fqs_to_alns
   \
  alns_to_procd_alns
```

Require inputs:
| Parameter | Allowed values | Explanation |
| ----- | ----- | ----- |
| FQS  | FASTQ channel | Channel with sample-level FASTQs |
| params.alignment\$manifest_to_alns\$fq_trim_tool | `fastp`, `trim_galore`, `trimmomatic` | FASTQ trimming tool |
| params.alignment\$manifest_to_alns\$fq_trim_tool_parameters | key:value hash (see documentation) | FASTQ trimming tool parameters |
| params.alignment\$manifest_to_alns\$aln_tool | `star`, `bwa`, `bbmap` | Alignment tool |
| params.alignment\$manifest_to_alns\$aln_tool_parameters | key:value hash (see documentation) | Alignment tool parameters |
| params.alignment\$manifest_to_alns\$aln_ref | A reference FASTA | Reference FASTA for alignment | 
| params.alignment\$manifest_to_alns\$gtf | A GTF | GTF used for informing alignments |
| params.alignment\$manifest_to_alns\$alt_ref | A reference FASTA | Alternate reference FASTA for alignment (e.g. transcriptomic ) |

Emitted channels:
| Channel Name | Explanation | 
| ----- | ----- |
| fqs | Raw FASTQS |
| procd_fqs | Process FASTQS |
| alns | Alignents |
| alt_alns | Alternate Alignments (e.g. --transcriptomeBAM from `STAR`) |
| junctions | Junctions, if generated |

# procd_fqs_to_alns

Given processed FASTQs, generate alignments using user-provided parameters.

Nested workflow design:
```
procd_fqs_to_alns
  \
 alns_to_procd_alns
```

Require inputs:
| Parameter | Allowed values | Explanation |
| ----- | ----- | ----- |
| PROCD_FQS  | FASTQ channel | Channel with sample-level processed FASTQs |
| params.alignment\$manifest_to_alns\$aln_tool | `star`, `bwa`, `bbmap` | Alignment tool |
| params.alignment\$manifest_to_alns\$aln_tool_parameters | key:value hash (see documentation) | Alignment tool parameters |
| params.alignment\$manifest_to_alns\$aln_ref | A reference FASTA | Reference FASTA for alignment | 
| params.alignment\$manifest_to_alns\$gtf | A GTF | GTF used for informing alignments |
| params.alignment\$manifest_to_alns\$alt_ref | A reference FASTA | Alternate reference FASTA for alignment (e.g. transcriptomic ) |

Emitted channels:
| Channel Name | Explanation | 
| ----- | ----- |
| alns | Alignents |
| alt_alns | Alternate Alignments (e.g. --transcriptomeBAM from `STAR`) |
| junctions | Junctions, if generated |

# alns_to_procd_alns

Given alignments, process alignments using GATK's best practices for alignment sanitization.

Require inputs:

| Parameter | Allowed values | Explanation |
| ----- | ----- | ----- |
| ALNS | Alignment files | Input alignment files |
| JUNCTIONS | Junctions | Input junctions (used for RNA InDel realignment) |
| VCFS | Valid v4.2 VCF | Input VCF (for RNA InDel realignment) |
| params.alignment\$alns_to_procd_alns\$aln_ref | A reference FASTA | Reference FASTA for alignment |
| params.alignment\$alns_to_procd_alns\$bed | A BED file | BED file with regions of interest (e.g. exons) |
| params.alignment\$alns_to_procd_alns\$gtf | A GTF | GTF used for informing InDel realignment |
| params.alignment\$alns_to_procd_alns\$dup_marker_tool | `picard2`,`bamsormadup` | Tool for marking duplicate reads |
| params.alignment\$alns_to_procd_alns\$dup_marker_tool_parameters | key:value hash (see documentation) | Duplicate marking tool parameters |
| params.alignment\$alns_to_procd_alns\$base_recalibrator_tool | `gatk4` | Base quality recalibration tool |
| params.alignment\$alns_to_procd_alns\$base_recalibrator_tool_parameters | key:value hash (see documentation) | Base quality recalibration tool parameters |
| params.alignment\$alns_to_procd_alns\$indel_realign_tool | `abra2`,`abra2_rna` | InDel realignment tool |
| params.alignment\$alns_to_procd_alns\$indel_realign_tool_parameters | key:value hash (see documentation) | InDel realignment tool parameters |
| params.alignment\$alns_to_procd_alns\$known_sites_ref | A VCF | Known sites VCF (used for base quality recalibration) |
| MANIFEST | RAFT Manifest | Provides sample-level information for processing |


# Implicit workflows
- make_ancillary_index_files
- bams_to_base_qual_recal_w_indices
